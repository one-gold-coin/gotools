package getenv

import (
	"fmt"
	"testing"
)

func Test_Get(t *testing.T) {
	// 初始化
	GetEnv().SetFilePath(".env.example").Init()
	// 获取值
	fmt.Println("val: ", GetVal("APP_ENV").String())
}